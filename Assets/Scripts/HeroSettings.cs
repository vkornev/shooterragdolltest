using UnityEngine;

[CreateAssetMenu(fileName = "New Hero Settings", menuName = "ScriptableObjects/HeroSettings", order = 1)]
public class HeroSettings : ScriptableObject
{
    public float damage;
    public float projectileSpeed;
    public float projectileForce;
    public float shootCooldown;
}
