using System;
using UnityEngine;

public class Projectile : PoolableBase
{
    public struct ProjectileData
    {
        public Vector3 position;
        public Quaternion rotation;
        public float speed;
        public float damage;
        public float force;
        public LayerMask hitMask;
    }

    public event Action<Projectile, Collider> OnTargetHit;

    private const float maxLifeTime = 2f;

    private ProjectileData data;

    private RaycastHit[] hits;
    private Vector3 previousPosition;
    private CharacterManager characterManager;
    private float timeStart;

    public Projectile(string prefabLink, ProjectileData data) : base(prefabLink)
    {
        this.data = data;
        hits = new RaycastHit[1];
    }

    public void Init(ProjectileData data, CharacterManager characterManager)
    {
        this.data = data;
        this.characterManager = characterManager;
    }

    public override void Activate(Vector3 position, Quaternion rotation)
    {
        base.Activate(position, rotation);
        previousPosition = position;
        timeStart = Time.time;
    }

    public void Move()
    {
        Vector3 projectileDir = TransformObj.forward;
        Vector3 direction = projectileDir * data.speed * Time.deltaTime;
        int hitCount = Physics.RaycastNonAlloc(
            previousPosition, projectileDir, hits, direction.magnitude, data.hitMask);
        TransformObj.position += direction;
        previousPosition = TransformObj.position;

        if (hitCount > 0)
        {
            Collider collider = hits[0].collider;
            Character character = characterManager.GetColliderHit(collider);
            character?.DoDamage(data.damage, projectileDir * data.force, hits[0].point);
            OnTargetHit?.Invoke(this, collider);
        }
        else if (Time.time - timeStart >= maxLifeTime)
            OnTargetHit?.Invoke(this, null);
    }
}