using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class Character : PoolableBase
{
    public struct CharacterData
    {
        public float maxHealth;
        public float lookRadius;
    }

    public struct BoneData
    {
        public Transform transform;
        public Rigidbody rigidbody;
    }

    public event Action<Character> OnDeath;
    public event Action<Character> OnDestinationReached;

    public bool IsDead { get { return Health <= 0f; } }

    public float Health { get; private set; }

    protected NavMeshAgent navAgent;
    protected CharacterView characterView;
    protected Dictionary<HumanBodyBones, BoneData> allBonesDict;
    protected CharacterData data;

    public Character(string prefabLink, CharacterData data) : base(prefabLink)
    {
        this.data = data;

        allBonesDict = new Dictionary<HumanBodyBones, BoneData>();
    }

    public virtual void TickAction()
    {
        if (!IsActive || IsDead)
            return;

        if (!navAgent.isStopped && navAgent.remainingDistance > navAgent.stoppingDistance)
            return;

        navAgent.ResetPath();
        OnDestinationReached?.Invoke(this);
    }

    public override void Activate(Vector3 position, Quaternion rotation)
    {
        base.Activate(position, rotation);
        SetRagdoll(false);
        navAgent.Warp(position);
        Health = data.maxHealth;
    }

    public virtual void DirectTo(Vector3 position)
    {
        navAgent.SetDestination(position);
    }

    public void DoDamage(float damage, Vector3 hitDirection, Vector3 hitPoint)
    {
        Health -= damage;

        if (Health > 0f)
            return;

        SetRagdoll(true);
        ApplyHit(hitDirection, hitPoint);
        OnDeath?.Invoke(this);
    }

    public virtual void GenerateDestination(Bounds bounds)
    {
        float x = Random.Range(bounds.min.x, bounds.max.x);
        float z = Random.Range(bounds.min.z, bounds.max.z);
        Vector3 dest = bounds.ClosestPoint(new Vector3(x, bounds.max.y, z));
        DirectTo(dest);
    }

    protected override void FinishedLoadingAsset(ResourceRequest request)
    {
        base.FinishedLoadingAsset(request);

        if (!gameObject)
            return;
        navAgent = gameObject.GetComponent<NavMeshAgent>();
        characterView = gameObject.GetComponentInChildren<CharacterView>();

        if (characterView != null)
            SetBonesDictionary();
    }

    private void SetRagdoll(bool isOn)
    {
        if (characterView == null)
            return;

        navAgent.enabled = !isOn;

        if (characterView.LocalAnimator != null)
            characterView.LocalAnimator.enabled = !isOn;
        if (characterView.LocalCollider != null)
            characterView.LocalCollider.enabled = !isOn;

        foreach (HumanBodyBones bone in allBonesDict.Keys)
            if (allBonesDict[bone].rigidbody != null)
                allBonesDict[bone].rigidbody.isKinematic = !isOn;
    }

    private void ApplyHit(Vector3 hitDirection, Vector3 hitPoint)
    {
        float minDistance = float.MaxValue;
        HumanBodyBones closestBone = HumanBodyBones.Hips;
        foreach (HumanBodyBones bone in allBonesDict.Keys)
        {
            BoneData boneData = allBonesDict[bone];
            if (boneData.rigidbody == null)
                continue;

            float boneDistance = (boneData.transform.position - hitPoint).sqrMagnitude;
            if (boneDistance < minDistance)
            {
                minDistance = boneDistance;
                closestBone = bone;
            }
        }

        allBonesDict[closestBone].rigidbody.AddForceAtPosition(hitDirection, hitPoint, ForceMode.Impulse);
    }

    private void SetBonesDictionary()
    {
        foreach (HumanBodyBones bone in Enum.GetValues(typeof(HumanBodyBones)))
        {
            if (bone == HumanBodyBones.LastBone)
                continue;
            Transform boneTransform = characterView.LocalAnimator.GetBoneTransform(bone);
            if (boneTransform == null)
                continue;

            Rigidbody boneRigidbody = boneTransform.GetComponent<Rigidbody>();

            BoneData boneData = new BoneData
            {
                rigidbody = boneRigidbody,
                transform = boneTransform
            };
            allBonesDict.Add(bone, boneData);
        }
    }
}
