using UnityEngine;

public class Hero : Character, IShooter
{
    private bool isMouseAction;
    private bool isShooting;

    private float cooldownCounter;
    private ShooterData shooterData;

    private Camera mainCamera;

    private RaycastHit[] hits;
    private Vector3 tapWorldPosition;
    private Transform rightHandTransform;

    public Hero(string prefabLink, CharacterData data) : base(prefabLink, data)
    {
        isMouseAction = false;
        InputManager.OnTapPressed += TapPressed;
        InputManager.OnTapRelease += TapRelease;

        mainCamera = Camera.main;
        hits = new RaycastHit[1];
        cooldownCounter = 0f;
    }

    ~Hero()
    {
        InputManager.OnTapPressed -= TapPressed;
        InputManager.OnTapRelease -= TapRelease;
    }

    public void SetShooterData(ShooterData data)
    {
        shooterData = data;
    }

    public void IsShootingEnabled(bool val)
    {
        isShooting = val;
    }

    public override void TickAction()
    {
        if (IsActive && isShooting)
            Shooting();
        else
            base.TickAction();
    }

    protected override void FinishedLoadingAsset(ResourceRequest request)
    {
        base.FinishedLoadingAsset(request);

        if (allBonesDict.ContainsKey(HumanBodyBones.RightHand))
            rightHandTransform = allBonesDict[HumanBodyBones.RightHand].transform;
    }

    private void TapPressed(Vector3 position)
    {
        ProcessTapPosition(position);
        isMouseAction = true;
    }

    private void TapRelease(Vector3 position)
    {
        isMouseAction = false;

        TapReleaseAction(position);
    }

    private void Shooting()
    {
        cooldownCounter -= Time.deltaTime;

        characterView?.SetIKGoalPosition(hits[0].point);

        if (!isMouseAction)
            return;

        characterView?.SetAimMode(true);

        if (cooldownCounter > 0)
            return;

        cooldownCounter = shooterData.shootCooldown;

        if (rightHandTransform == null)
            return;

        Quaternion projectileRotation = rightHandTransform.rotation;
        projectileRotation = Quaternion.LookRotation((hits[0].point - rightHandTransform.position).normalized);

        Projectile.ProjectileData data = new Projectile.ProjectileData
        {
            damage = shooterData.projectileDamage,
            speed = shooterData.projectileSpeed,
            force = shooterData.projectileForce,
            position = rightHandTransform.position,
            rotation = projectileRotation,
            hitMask = shooterData.shootingMask
        };
        shooterData.projectileManager.RequestLaunchProjectile(data);
    }

    private void TapReleaseAction(Vector3 position)
    {
        characterView?.SetAimMode(false);

        if (isShooting)
            return;
        int count = ProcessTapPosition(position);

        if (count == 0)
            return;

        DirectTo(hits[0].point);
    }

    private int ProcessTapPosition(Vector3 position)
    {
        Ray ray = mainCamera.ScreenPointToRay(position);
        LayerMask layerMask = shooterData.walkingMask;
        int count = Physics.RaycastNonAlloc(ray, hits, mainCamera.farClipPlane, layerMask);
        return count;
    }
}
