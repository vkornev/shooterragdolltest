using UnityEngine;

public class Enemy : Character, IFollower
{
    private Character target;

    public Enemy(string prefabLink, CharacterData data) : base(prefabLink, data)
    {
    }

    public override void Deactivate()
    {
        characterView?.SetAimMode(false);
        base.Deactivate();
    }

    public void SetFollowTarget(Character target)
    {
        this.target = target;
    }

    public override void GenerateDestination(Bounds bounds)
    {
        bool isAggroTarget = target != null &&
            (target.TransformObj.position - TransformObj.position).magnitude <= data.lookRadius;

        if (isAggroTarget)
        {
            characterView?.SetAimMode(true);
            DirectTo(target.TransformObj.position);
            return;
        }

        characterView?.SetAimMode(false);

        base.GenerateDestination(bounds);
    }
}
