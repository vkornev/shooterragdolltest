using UnityEngine;

public class HeroManager : MonoBehaviour
{
    [SerializeField] private CharacterManager characterManager;
    [SerializeField] private ProjectileManager projectileManager;
    [SerializeField] private CameraFollower cameraFollower;
    [SerializeField] private Transform heroSpawnPlace;
    [SerializeField] private LayerMask shootingMask;
    [SerializeField] private LayerMask walkingMask;
    [SerializeField] private HeroSettings settings;

    private Hero hero;

    private void Start()
    {
        hero = characterManager.SpawnHero(heroSpawnPlace.position, heroSpawnPlace.rotation, HeroLoaded);
    }

    private void Update()
    {
        hero.TickAction();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other != hero.ColliderObj)
            return;

        hero.IsShootingEnabled(true);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other != hero.ColliderObj)
            return;

        hero.IsShootingEnabled(false);
    }

    private void HeroLoaded()
    {
        hero.OnReady -= HeroLoaded;
        hero.Activate(heroSpawnPlace.position, heroSpawnPlace.rotation);
        hero.SetShooterData(new ShooterData
        {
            projectileDamage = settings.damage,
            projectileSpeed = settings.projectileSpeed,
            projectileForce = settings.projectileForce,
            shootCooldown = settings.shootCooldown,
            projectileManager = projectileManager,
            shootingMask = shootingMask,
            walkingMask = walkingMask
        });

        cameraFollower.SetTarget(hero.TransformObj);
    }
}
