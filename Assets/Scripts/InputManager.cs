﻿using System;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public static event Action<Vector3> OnTapPressed;
    public static event Action<Vector3> OnTapRelease;

    private void Update()
    {
        if (Input.GetMouseButton(0))
            OnTapPressed?.Invoke(Input.mousePosition);

        if (Input.GetMouseButtonUp(0))
            OnTapRelease?.Invoke(Input.mousePosition);
    }
}
