using System.Collections.Generic;
using UnityEngine;

public class ProjectileManager : MonoBehaviour
{
    [SerializeField] private CharacterManager characterManager;
    [SerializeField] private string prefabLink;

    private Queue<Projectile> projectilesPool;
    private List<Projectile> allProjectiles;

    private void Awake()
    {
        projectilesPool = new Queue<Projectile>();
        allProjectiles = new List<Projectile>();
    }

    private void Update()
    {
        foreach (Projectile projectile in allProjectiles)
            if (projectile.IsActive)
                projectile.Move();
    }

    public void RequestLaunchProjectile(Projectile.ProjectileData data)
    {
        if (projectilesPool.Count == 0)
        {
            Projectile newProjectile = new Projectile(prefabLink, data);
            projectilesPool.Enqueue(newProjectile);
            allProjectiles.Add(newProjectile);
        }

        Projectile projectile = projectilesPool.Dequeue();
        if (projectile.IsReady)
            InitProjectile(data, projectile);
        else
            projectile.OnReady += ProjectileReady;

        void ProjectileReady()
        {
            projectile.OnReady -= ProjectileReady;
            InitProjectile(data, projectile);
        }
    }

    private void InitProjectile(Projectile.ProjectileData data, Projectile projectile)
    {
        projectile.Init(data, characterManager);
        projectile.Activate(data.position, data.rotation);
        projectile.OnTargetHit += TargetHit;
    }

    private void TargetHit(Projectile projectile, Collider target)
    {
        projectile.OnTargetHit -= TargetHit;
        projectile.Deactivate();
        projectilesPool.Enqueue(projectile);
    }
}
