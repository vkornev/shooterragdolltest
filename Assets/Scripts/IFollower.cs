public interface IFollower
{
    void SetFollowTarget(Character target);
}
