using UnityEngine;

public struct ShooterData
{
    public ProjectileManager projectileManager;
    public float projectileSpeed;
    public float projectileDamage;
    public float projectileForce;
    public float shootCooldown;
    public LayerMask walkingMask;
    public LayerMask shootingMask;
}

public interface IShooter
{
    void SetShooterData(ShooterData data);
    void IsShootingEnabled(bool val);
}
