using UnityEngine;
using UnityEngine.AI;

public class CharacterView : MonoBehaviour
{
    public Animator LocalAnimator => animator;
    public Collider LocalCollider => localCollider;

    [SerializeField] private Animator animator;
    [SerializeField] private Collider localCollider;
    [SerializeField] private NavMeshAgent navAgent;
    [SerializeField] private float IKSwitchSpeed;
    [SerializeField] private Transform rigtHandOrigin;
    [SerializeField] private Transform leftHandOrigin;
    [SerializeField] private Transform rightHandIKPlace;
    [SerializeField] private Transform leftHandIKPlace;
    [SerializeField] private float IKReachDistance;

    private int speedValueId, deathTriggerId;

    private bool isAimMode;
    private float aimIKWeight;

    public void SetAimMode(bool isAimMode)
    {
        this.isAimMode = isAimMode;
    }

    public void SetIKGoalPosition(Vector3 target)
    {
        Debug.DrawLine(rigtHandOrigin.position, target, Color.red, Time.deltaTime);
        if (rightHandIKPlace != null)
            rightHandIKPlace.position =
                rigtHandOrigin.position + (target - rigtHandOrigin.position).normalized * IKReachDistance;
        if (leftHandIKPlace != null)
            leftHandIKPlace.position =
                leftHandOrigin.position + (target - rigtHandOrigin.position).normalized * IKReachDistance;
    }

    private void Start()
    {
        isAimMode = false;
        aimIKWeight = 0f;

        speedValueId = Animator.StringToHash("Speed");
        deathTriggerId = Animator.StringToHash("Death");
    }

    private void Update()
    {
        UpdateMove();
    }

    private void OnAnimatorIK(int layerIndex)
    {
        UpdateIK();
    }

    private void UpdateIK()
    {
        aimIKWeight = Mathf.Lerp(aimIKWeight, isAimMode ? 1f : 0f, IKSwitchSpeed * Time.deltaTime);

        animator.SetIKPositionWeight(AvatarIKGoal.RightHand, aimIKWeight);
        animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, aimIKWeight);

        if (rightHandIKPlace != null)
            animator.SetIKPosition(AvatarIKGoal.RightHand, rightHandIKPlace.position);
        if (leftHandIKPlace != null)
            animator.SetIKPosition(AvatarIKGoal.LeftHand, leftHandIKPlace.position);
    }

    private void UpdateMove()
    {
        animator.SetFloat(speedValueId, navAgent.velocity.magnitude);
    }
}
