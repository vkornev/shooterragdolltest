using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class CharacterManager : MonoBehaviour
{
    [SerializeField] private int maxCharactersAvailable;
    [SerializeField] private Transform finish;
    [SerializeField] private float maxHealth;
    [SerializeField] private float lookRadius;
    [SerializeField] private string enemyPrefabLink;
    [SerializeField] private string heroPrefabLink;
    [SerializeField] private Collider walkableBorders;
    [SerializeField] private float deathDelay;

    private Queue<Character> unitPool;
    private Dictionary<Collider, Character> spawnedUnits;
    private Hero hero;

    public Hero SpawnHero(Vector3 position, Quaternion rotation, Action heroLoadedCallback)
    {
        hero = new Hero(heroPrefabLink, new Character.CharacterData
        {
            maxHealth = maxHealth,
            lookRadius = lookRadius
        });
        hero.OnReady += () =>
        {
            hero.OnReady -= heroLoadedCallback;
            foreach (Collider c in spawnedUnits.Keys)
                if (spawnedUnits[c] is Enemy)
                    ((Enemy)spawnedUnits[c]).SetFollowTarget(hero);
            heroLoadedCallback();
        };
        return hero;
    }

    public void SpawnEnemy(Vector3 position, Quaternion rotation)
    {
        Character character;
        if (unitPool.Count == 0 && spawnedUnits.Count < maxCharactersAvailable)
            CreateEnemy();

        if (unitPool.Count == 0)
            return;

        character = unitPool.Dequeue();
        if (character.IsReady)
            InsertUnit(character, position, rotation);
        else
        {
            character.OnReady += () =>
            {
                InsertUnit(character, position, rotation);
            };
        }
    }

    public Character GetColliderHit(Collider collider)
    {
        if (!spawnedUnits.ContainsKey(collider))
            return null;

        return spawnedUnits[collider];
    }

    public Character GetRunner(Collider collider)
    {
        if (!spawnedUnits.ContainsKey(collider))
            return null;

        return spawnedUnits[collider];
    }

    private void Awake()
    {
        unitPool = new Queue<Character>();
        spawnedUnits = new Dictionary<Collider, Character>();
    }

    private void Update()
    {
        TickThroughAllCharacters();
    }

    private void OnDestroy()
    {
        foreach (Character character in spawnedUnits.Values)
        {
            character.OnDeath -= CharatcerDeath;
            character.OnDestinationReached -= NeedNewDestination;
        }
    }

    private void TickThroughAllCharacters()
    {
        foreach (Character character in spawnedUnits.Values)
            character.TickAction();
    }

    private void CreateEnemy()
    {
        Enemy unit = new Enemy(enemyPrefabLink, new Character.CharacterData
        {
            maxHealth = maxHealth,
            lookRadius = lookRadius
        });
        unit.SetFollowTarget(hero);
        unit.OnDeath += CharatcerDeath;
        unit.OnDestinationReached += NeedNewDestination;
        unitPool.Enqueue(unit);
    }

    private void InsertUnit(Character unit, Vector3 position, Quaternion rotation)
    {
        unit.Activate(position, rotation);
        spawnedUnits.Add(unit.ColliderObj, unit);
    }

    private void NeedNewDestination(Character unit)
    {
        unit.GenerateDestination(walkableBorders.bounds);
    }

    private void CharatcerDeath(Character unit)
    {
        if (!spawnedUnits.ContainsKey(unit.ColliderObj))
            return;

        StartCoroutine(WaitAndDespawn(unit.ColliderObj));
    }

    private IEnumerator WaitAndDespawn(Collider unitCollider)
    {
        yield return new WaitForSeconds(deathDelay);

        Character character = spawnedUnits[unitCollider];
        character.Deactivate();
        spawnedUnits.Remove(unitCollider);
        unitPool.Enqueue(character);
    }
}
