using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] private CharacterManager characterManager;
    [SerializeField] private bool automaticSpawn;
    [SerializeField] private float spawnDelay;
    [SerializeField] private Transform defaultSpawnPoint;

    private float timer;

    private void Update()
    {
        if (!automaticSpawn)
            return;

        timer -= Time.deltaTime;

        if (timer > 0f)
            return;

        timer = spawnDelay;
        characterManager.SpawnEnemy(defaultSpawnPoint.position, defaultSpawnPoint.rotation);
    }

}
