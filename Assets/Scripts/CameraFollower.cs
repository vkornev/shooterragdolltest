using UnityEngine;

public class CameraFollower : MonoBehaviour
{
    [SerializeField] private float followSpeed;

    private Transform target;

    public void SetTarget(Transform target)
    {
        this.target = target;
    }

    private void Update()
    {
        if (target == null)
            return;

        transform.position =
            Vector3.Lerp(transform.position, target.position, followSpeed * Time.deltaTime);
    }
}
